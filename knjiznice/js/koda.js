
var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";

var currentEhrId = "";
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/*
*	1 - zdrav sportnik
*	2 - starejsa gospa/bolana oseba
*	3 - poskodovan otrok
*/
var backupPodatki = [
	{ teze : [79, 70, 77, 71, 73, 71, 74, 77, 75, 72, 73, 78, 73, 70, 78, 75, 79, 79, 77, 74, 76, 71, 77, 76, 74, 70, 72, 73, 79, 72, 70, 70, 74, 79, 73, 79, 71, 77, 79, 76, 72, 72, 76, 75, 72, 74, 73, 
			72, 73, 70, 78, 72, 73, 76, 78, 78, 78, 72, 71, 79, 76, 73, 79, 72, 72, 75, 71, 73, 72, 70, 71, 74, 72, 78, 72, 77, 74, 77, 71, 70, 77, 71, 72, 72, 77, 70, 70, 78, 74, 73, 79, 70, 76, 71, 72, 71, 76, 75, 74, 78 ], 
telesnaTemp: [36.6, 37.8, 36.0, 37.1, 37.2, 37.4, 36.8, 37.4, 36.4, 37.3, 36.4, 36.6, 36.3, 37.4, 36.2, 36.3, 36.6, 37.3, 37.2, 37.4, 37.9, 37.5, 37.9, 37.4, 37.6, 
				37.9, 37.4, 37.0, 36.6, 36.4, 36.0, 37.2, 36.9, 37.2, 37.8, 37.4, 36.0, 36.2, 36.6, 37.0, 37.3, 36.5, 36.9, 37.1, 36.5, 36.6, 36.0, 36.3, 36.6, 37.5, 37.4, 37.1, 36.3, 37.0, 37.4, 36.6, 36.5, 36.8, 37.1, 
				37.8, 36.0, 36.0, 36.0, 37.8, 37.2, 37.6, 36.0, 37.1, 37.4, 36.4, 36.5, 36.2, 36.9, 37.5, 37.7, 36.6, 36.2, 37.5, 36.2, 36.7, 36.9, 37.3, 37.1, 36.6, 36.0, 37.9, 37.9, 37.5, 37.9, 37.3, 36.5, 37.6, 36.2, 37.8, 
				37.8, 37.2, 37.3, 36.6, 37.8, 36.1],
zgornjiPritisk: [120, 128, 134, 129, 129, 130, 122, 133, 130, 128, 128, 122, 120, 132, 134, 122, 123, 121, 131, 121, 132, 127, 124, 121, 134, 131, 
				134, 120, 121, 132, 120, 128, 125, 134, 122, 126, 121, 131, 124, 123, 124, 124, 125, 124, 122, 125, 133, 132, 126, 121, 134, 131, 120, 130, 124, 120, 127, 
				123, 120, 120, 127, 127, 128, 132, 134, 123, 131, 120, 134, 127, 124, 124, 132, 121, 120, 134, 133, 126, 131, 132, 127, 122, 128, 128, 133, 124, 120, 125, 127, 120, 132, 120, 120, 125, 124, 134, 128, 120, 134, 120],
spodnjiPritisk: [74, 84, 83, 84, 83, 73, 75, 71, 75, 73, 76, 71, 71, 83, 73, 71, 75, 73, 77, 79, 73, 83, 72, 74, 74, 76, 77, 70, 79, 74, 79, 76, 73, 84, 75, 78, 72, 80, 80, 84, 75, 71, 77, 76, 84,
				 80, 77, 81, 76, 84, 76, 71, 75, 70, 75, 71, 76, 74, 72, 78, 70, 81, 84, 74, 72, 81, 74, 74, 76, 84, 80, 81, 77, 72, 72, 84, 75, 72, 80, 81, 71, 78, 82, 83, 79, 80, 70, 77, 76, 72
				, 77, 77, 75, 76, 81, 77, 72, 77, 73, 70]
		
	},
	{
		teze : [79, 70, 77, 71, 73, 71, 74, 77, 75, 72, 73, 78, 73, 70, 78, 75, 79, 79, 77, 74, 76, 71, 77, 76, 74, 70, 72, 73, 79, 72, 70, 70, 74, 79, 73, 79, 71, 77, 79, 76, 72, 72, 76, 75, 72, 74, 73, 
				72, 73, 70, 78, 72, 73, 76, 78, 78, 78, 72, 71, 79, 76, 73, 79, 72, 72, 75, 71, 73, 72, 70, 71, 74, 72, 78, 72, 77, 74, 77, 71, 70, 77, 71, 72, 72, 77, 70, 70, 78, 74, 73, 79, 70, 76, 71, 72, 71, 76, 75, 74, 78 ], 
		telesnaTemp: [36.4, 38.7, 38.9, 37.5, 38.3, 37.1, 38.3, 40.8, 36.0, 36.4, 36.0, 36.0, 37.3, 39.5, 40.1, 38.9, 39.9, 39.7, 36.5, 38.7, 39.0, 36.6, 37.0, 40.8, 38.1, 38.4, 38.2, 40.3, 39.5, 39.7,
					 37.3, 37.6, 38.5, 39.3, 36.5, 36.0, 39.1, 39.4, 39.5, 38.7, 37.7, 38.0, 37.8, 39.9, 39.4, 37.2, 38.5, 39.1, 36.2, 40.2, 40.7, 40.7, 36.3, 39.5, 39.8, 38.6, 36.6, 38.8, 36.0, 39.0, 37.6, 38.5, 37.1, 38.3, 36.3, 38.4, 38.2, 40.2, 
					 36.7, 40.5, 36.4, 37.2, 36.8, 37.7, 37.1, 36.7, 38.2, 36.9, 36.2, 40.2, 36.2, 38.0, 40.1, 38.1, 39.4, 37.6, 39.9, 36.0, 39.1, 39.1, 38.0, 39.2, 40.5, 39.0, 38.7, 38.5, 40.1, 36.3, 38.9, 37.3],
		zgornjiPritisk: [131, 147, 144, 150, 147, 135, 137, 139, 134, 142, 141, 145, 144, 130, 152, 154, 142, 140, 150, 151, 142, 159, 149, 156, 137, 143, 131, 147, 158, 159, 149, 151, 146, 155, 141, 155,
						 130, 140, 156, 157, 152, 138, 134, 159, 130, 148, 153, 142, 159, 143, 156, 141, 142, 137, 137, 141, 151, 130, 151, 141, 159, 140, 132, 138, 157, 135, 133, 150, 145, 130, 147, 159,
						 130, 143, 158, 130, 131, 144, 143, 130, 149, 139, 134, 154, 138, 133, 135, 159, 134, 156, 132, 155, 158, 134, 133, 148, 131, 159, 138, 138],
		spodnjiPritisk: [97, 106, 113, 112, 107, 86, 91, 95, 107, 96, 114, 100, 102, 106, 92, 110, 86, 91, 99, 109, 101, 106, 106, 99, 103, 108, 100, 94, 86, 86, 109, 98, 99, 107, 96, 91, 100, 94, 93, 85,
						 105, 92, 100, 114, 105, 100, 102, 106, 106, 108, 101, 114, 99, 114, 90, 87, 100, 106, 89, 101, 99, 105, 106, 105, 98, 109, 111, 105, 88, 112, 105, 101, 111, 113, 100, 102, 90, 109,
						 85, 111, 103, 93, 103, 87, 85, 108, 112, 100, 99, 86, 108, 113, 106, 99, 103, 111, 86, 92, 94, 89]
	},
	{
		teze: [55, 56, 54, 59, 56, 57, 56, 50, 59, 51, 55, 50, 58, 55, 54, 58, 53, 56, 54, 56, 57, 53, 53, 52, 53, 56, 52, 59, 51, 58, 59, 58, 54, 56, 58, 52, 53, 56, 52, 54, 59, 59, 54, 58, 56,
				50, 58, 59, 56, 54, 58, 53, 57, 53, 57, 53, 59, 51, 52, 51, 59, 53, 51, 55, 59, 51, 59, 52, 58, 53, 56, 57, 54, 52, 57, 51, 52, 55, 52, 51, 59, 50, 56, 59, 56, 56, 52, 55, 57, 56, 58, 59, 59, 50, 54, 59, 51, 54, 53, 59],
		telesnaTemp: [36.6, 37.8, 36.0, 37.1, 37.2, 37.4, 36.8, 37.4, 36.4, 37.3, 36.4, 36.6, 36.3, 37.4, 36.2, 36.3, 36.6, 37.3, 37.2, 37.4, 37.9, 37.5, 37.9, 37.4, 37.6, 
					37.9, 37.4, 37.0, 36.6, 36.4, 36.0, 37.2, 36.9, 37.2, 37.8, 37.4, 36.0, 36.2, 36.6, 37.0, 37.3, 36.5, 36.9, 37.1, 36.5, 36.6, 36.0, 36.3, 36.6, 37.5, 37.4, 37.1, 36.3, 37.0, 37.4, 36.6, 36.5, 36.8, 37.1, 
					37.8, 36.0, 36.0, 36.0, 37.8, 37.2, 37.6, 36.0, 37.1, 37.4, 36.4, 36.5, 36.2, 36.9, 37.5, 37.7, 36.6, 36.2, 37.5, 36.2, 36.7, 37.1, 36.8, 37.4, 36.8, 37.4, 38.6, 37.5, 36.0, 37.2, 37.8, 
					37.1, 37.6, 37.3, 37.2, 36.5, 37.8, 37.5, 38.9, 37.8, 38.9],
		zgornjiPritisk: [146, 134, 138, 138, 154, 134, 125, 134, 143, 122, 154, 139, 121, 127, 124, 151, 152, 133, 144, 151, 147, 137, 145, 141, 151, 138, 126, 129, 146, 147, 148, 137, 138, 131, 132, 137,
						 146, 138, 152, 134, 152, 151, 130, 130, 135, 135, 138, 132, 125, 128, 129, 129, 122, 131, 150, 153, 149, 133, 139, 140, 137, 133, 134, 120, 121, 147, 138, 147, 142, 147, 138, 151
						, 143, 149, 126, 123, 141, 121, 148, 146, 129, 134, 121, 131, 145, 128, 141, 139, 142, 138, 136, 124, 151, 150, 125, 129, 154, 120, 122, 153],
		spodnjiPritisk: [95, 96, 91, 89, 86, 95, 91, 94, 83, 93, 98, 88, 88, 83, 85, 83, 96, 95, 87, 94, 94, 97, 80, 96, 89, 89, 82, 94, 81, 81, 93, 97, 98, 96, 86, 96, 91, 89, 90, 86, 94, 80, 94, 82, 83,
						92, 97, 92, 87, 84, 86, 93, 94, 98, 89, 83, 99, 91, 90, 80, 84, 95, 97, 82, 92, 95, 98, 95, 97, 81, 82, 91, 81, 88, 86, 97, 92, 83, 81, 99, 88, 99, 92, 94, 97, 93, 97, 96, 96, 87, 96, 81, 
						83, 86, 95, 87, 93, 86, 82, 90]
	}
];


/*
*
*
*/
function razlikaDni(date1, date2){
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	return diffDays;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	  var ehrIdNr = "";
	  var sessionId = getSessionId();
	  
	  var ime;
	  var priimek;
	  var datumRojstva;
	  
	  switch (stPacienta) {
	  	case 0:
	  		ime = "Janko";
	  		priimek = "Športnik";
	  		datumRojstva = "1987-11-21T11:40Z";
	  		break;
	  	case 1:
	  		ime = "Micka";
	  		priimek = "Stara";
	  		datumRojstva = "1920-02-28T11:40Z";
	  		break;
	  	case 2:
	  		ime = "Miha";
	  		priimek = "Poškodovan";
	  		datumRojstva = "2003-06-14T11:40Z";
	  		break;
	  	default:
	  		// code
	  }
	  	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		    	$("#preberiObstojeciEHR option")[stPacienta + 1].value = data.ehrId;
		        ehrIdNr = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrIdNr}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   for(var zapis = 0; zapis < 100; zapis++){
		                   	var datumInUra = new Date(datumRojstva);
		                   	
		                   	var danes = new Date();
		                   	var dniMedDatumoma = razlikaDni(datumInUra, danes);
		                   	dniMedDatumoma = Math.ceil(dniMedDatumoma / 100) * zapis;
		                   	
		                   	datumInUra.setDate(datumInUra.getDate() + dniMedDatumoma);
		                   	
		                   	var teza = backupPodatki[stPacienta]["teze"][zapis];
		                   	var telesnaTemperatura = backupPodatki[stPacienta]["telesnaTemp"][zapis];
		                   	var sistolicniPritisk = backupPodatki[stPacienta]["zgornjiPritisk"][zapis];
		                   	var distolicniPritisk = backupPodatki[stPacienta]["spodnjiPritisk"][zapis];
		                   	$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
								// Struktura predloge je na voljo na naslednjem spletnem naslovu:
					      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra.toISOString(),
							    "vital_signs/body_weight/any_event/body_weight": teza,
							   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
							    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
							    "vital_signs/blood_pressure/any_event/systolic": sistolicniPritisk,
							    "vital_signs/blood_pressure/any_event/diastolic": distolicniPritisk
							};
							var parametriZahteve = {
							    ehrId: ehrIdNr,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: "Franci"
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        $("#dodajMeritveVitalnihZnakovSporocilo").html(
					              "<span class='obvestilo label label-success fade-in'>" +
					              res.meta.href + ".</span>");
							    },
							    error: function(err) {
							    	alert(JSON.parse(err.responseText).userMessage + "'!");
							    }
							});
		                   }
		                }
		            },
		            error: function(err) {
		            	$("#preberiSporocilo").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka !");
		            }
		        });
		    }
		});
	  
	  return $("#preberiObstojeciEHR option")[stPacienta + 1].value;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	var sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
        			currentEhrId = ehrId;
			        $("#pacientName").text(party.firstNames + " " + party.lastNames);
                    $("#pacientBirthDate").text(party.dateOfBirth);
	    	    	$("#pacientControlPanel").show();
	    	},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function bloodPressureGraph(){
	
	//TODO: kreiraj podatke za graf o pritisku
	preberiMeritveVitalnihZnakov("pritisk");
	if(!$("#graphDiv").is(":visible")){
		$("#graphDiv").show();
	}
	
}

function temperatureGraph(){
	
	preberiMeritveVitalnihZnakov("telesna temperatura");	
	if(!$("#graphDiv").is(":visible")){
		$("#graphDiv").show();
	}
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov(tipPodatkov) {
	var sessionId = getSessionId();

	var ehrId = currentEhrId;
	var tip = tipPodatkov;
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#graphDiv").html("<span class='obvestilo " + "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature?limit=100",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	console.log(res);
					    	var options = {
					    		high: 42,
					    		low: 30
					    	};
					    	if (res.length > 0) {
					    		$("#graphDiv").html('<h2>Graf telesne temparature</h2><div class="ct-chart ct-perfect-fourth"></div>');
						        var data = Array();
						        data["labels"] = Array();
						        data["series"] = Array();
						        data["series"][0] = Array();
						        for (var i in res) {
						            var datum = new Date(res[i].time);
						            data["labels"][i] = (i % 10 == 0) ? datum.toLocaleDateString() : undefined;
						            data["series"][0][i] = res[i].temperature;
						        }
						        
						        new Chartist.Line('.ct-chart', data, options);
					    	}
					    },
					    error: function() {
					    	console.log("Eror");
					    	$("#graphDiv").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + "'!");
					    }
					});
				} else if (tip == "pritisk") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure?limit=100",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var options = {
					    		high: 170,
					    		low: 30
					    	};
					    	if (res.length > 0) {
					    		$("#graphDiv").html('<h2>Graf krvnega pritiska</h2><div class="ct-chart ct-perfect-fourth"></div>');
						        var data = Array();
						        data["labels"] = Array();
						        data["series"] = Array();
						        data["series"][0] = Array();
						        data["series"][1] = Array();
						        for (var i in res) {
						        	var datum = new Date(res[i].time);
						            data["labels"][i] = (i % 10 == 0) ? datum.toLocaleDateString() : undefined;
						            data["series"][0][i] = res[i].systolic;
						            data["series"][1][i] = res[i].diastolic;
						        }
						        
						        new Chartist.Line('.ct-chart', data, options);
					    	}
					    },
					    error: function() {
					    	
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		console.log("Eror");
	    		$("#temperatureGraph").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

$(document).ready(function() {
    
    /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#medication").empty();
		$("#results").html("");
		$("#pacientControlPanel").hide();
		$("#graphDiv").hide();
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
		
	});
	
	$("#pacientControlPanel").hide();
	
	$("#generateDataButton").on('click', function(){
		for(var i = 0; i < 3; i++){
			var sifra = generirajPodatke(i);
			console.log(sifra);
			
		}
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Novi pacienti so bili vnešeni. Njihovi ID-ji so že bili posodobljeni v seznamu.</span>");
	});
	
});

/*global $*/
