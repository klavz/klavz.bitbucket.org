/*global $*/

var MEJNA_TEMPERATURA = 37.0;
var MEJNI_SISTOLICNI = 140;
var MEJNI_DIASTOLICNI = 95;

function predlagajZdravila(){
    var sessionId = getSessionId();
    var ehrId = currentEhrId;
    $("#results").html("");
    $("#medication").empty();
    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature?limit=100",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
        	if (res.length > 0) {
        		var povprecnaTemeperatura = izracunajPovprecje(res.map(a => a.temperature));
        		if(povprecnaTemeperatura > MEJNA_TEMPERATURA){
        		    $("#results").append('<div class="alert alert-danger" role="alert">POZOR! Vaša povprečna ' +
        		    'temperatura presega mejne vrednosti: ' + Number(povprecnaTemeperatura).toFixed(2) + ' (meja:' + MEJNA_TEMPERATURA + ')! Spodaj so predlagana zdravila.' + 
        		    '</div>');
        		    $("#medication").append('<li class="list-group-item list-group-item-action active"><strong><h4>Zdravila za povišano telesno temperaturo</h4></strong></li>');
        		    prikaziZdravilaZa("povisana telesna temperatura");
        		}
        	}
        	$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure?limit=100",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			        if (res.length > 0) {
                		var povprecniPritiskSistolicni = izracunajPovprecje(res.map(a => a.systolic));
                		var povprecniPritiskDiastolicni = izracunajPovprecje(res.map(a => a.diastolic));
                		if(povprecniPritiskDiastolicni > MEJNI_DIASTOLICNI || povprecniPritiskSistolicni > MEJNI_SISTOLICNI){
                		    $("#results").append('<div class="alert alert-danger" role="alert">POZOR! Vaš povprečni ' +
                    		    'krvni pritisk presega mejne vrednosti: ' + Number(povprecniPritiskSistolicni).toFixed(2) + 
                    		    '/' + Number(povprecniPritiskDiastolicni).toFixed(2) + ' (meja: ' + MEJNI_SISTOLICNI + '/' + MEJNI_DIASTOLICNI + ')! Spodaj so predlagana zdravila.' + 
                    		    '</div>');
                    		$("#medication").append('<li class="list-group-item list-group-item-action active"><strong><h4>Zdravila za povišan krvni tlak</h4></strong></li>');
                    		prikaziZdravilaZa("povisan krvni pritisk");
                		}
                	}
			    },
			    error: function() {
			    	
			    }
			});
        },
        error: function() {
        	console.log("Eror");
        	$("#graphDiv").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + "'!");
        }
    });
}

function izracunajPovprecje(vrednosti){
    var povprecje = 0;
    for(var i in vrednosti){
        povprecje += vrednosti[i];
    }
    povprecje /= vrednosti.length;
    
    return povprecje;
}

function prikaziZdravilaZa(simptomi){
    var urlNaslov = "knjiznice/lekarnar/";
    if(simptomi == "povisana telesna temperatura"){
        urlNaslov += "lekarnarTemp.html";  
    } else {
        urlNaslov += "lekarnarPressure.html";  
    }
    $.ajax({
	    url: urlNaslov, // "http://www.lekarnar.com/izdelki?keywords=" + encodeURIComponent(simptomi),
	    type: 'GET',
	    //headers: {"Access-Control-Allow-Credentials": true},
	    success: function (spletnaStran) {
	        // Zaradi errorja Access-Control-Allow-Origin denied nisem mogel izvest CORS Requesta in sem uproabil statične datoteke
	        // aplikacija bi morala delovati tudi v primeru da bi lahko pridobival speltne strani direktno iz njega
	        spletnaStran = $(spletnaStran).find("#products li").slice(0,3);
	        $(spletnaStran).each(function(){
	            var newSrc = "https://www.lekarnar.com" + $(this).find("img").attr("src");
	            var description = $(this).find("p").text();
	            var newLink = "https://www.lekarnar.com" + $(this).find("a").attr("href");
	            console.log(description);
	            $(this).empty();
	            $(this).addClass("list-group-item clearfix");
	            $(this).append('<img class="pull-left" src="' + newSrc + '" />');
	            $(this).append('<p class="lead" >' + description + "</p>");
	            $(this).append('<a href ="' + newLink + '"><button class="btn btn-info btn-md">Za več info in za nakup obiščite Lekarnar.com</button></a>');
	        });
	        $("#medication").append(spletnaStran);
	        
	    },
	    error: function() {
	    	console.log("Error");
	    }
	});
}